var data_file = "csv/data_directv.csv";

var width = 960,
    height = 600;

var projection = d3.geo.albersUsa()
    .scale(1100)
    .translate([width / 2, height / 2]);

var path = d3.geo.path().projection(projection);

var svg = d3.select("#map-container").append("svg")
    .attr("width", width)
    .attr("height", height);

var g = svg.append("g");

g.append( "rect" )
  .attr("width",width)
  .attr("height",height)
  .attr("fill","white")
  .attr("opacity",0)
  .on("mouseover",function(){
    hoverData = null;
    if ( probe ) probe.style("display","none");
  })

var map = g.append("g")
    .attr("id","map");

var probe,
    hoverData;

var dateScale, sliderScale, slider;

var date_format = d3.time.format("%b %d"),
    orderedColumns = [],
    currentFrame = 0,
    interval,
    frameLength = 500,
    isPlaying = false;

var sliderMargin = 65;

function circleSize(d){
  return Math.sqrt( .02 * Math.abs(d) );
};

d3.json("dmas.json", function(error, dma) {
  var nielsen = dma.objects.nielsen_dma.geometries;
  var dmaData = [];

  probe = d3.select("#map-container").append("div").attr("id","probe");

  var maxIdx = -1;
  var minIdx = 10000;

  // dma.objects.nielsen_dma.geometries = nielsen;  // what this do?
  var features = topojson.feature(dma, dma.objects.nielsen_dma).features;

  // Now read the CSV pt. 1
  d3.csv(data_file, function(intab_data) {

    // Get colum nnames
    var first = intab_data[0];
    for ( var col in first ) {
      if ( col != "dma_id" ) {
        orderedColumns.push(col);
      }
    }

    // make slider
    dateScale = createDateScale(orderedColumns).range([0,500]);

    // Attach CSV data to features
    var dma_id;
    for (var i in intab_data) {
      dma_id = intab_data[i].dma_id;

      // Get the DMA feature that this row corresponds to
      feature = features.filter(x => x.id.toString() === dma_id)[0];
      if (typeof(feature) !== "undefined") {
        feature["data"] = intab_data[i];
      }
    }

    // This creates the map elements and binds data to them
    map.selectAll("path")
      .data(features)
      .enter()
      .append("path")
      .attr("vector-effect","non-scaling-stroke")
      .attr("class","land")
      .attr("d", path)
      .attr("fill", d3.rgb(030, 030, 030));

    // This draws everything
    map.append("path", ".graticule")
      .attr("id", "dma-borders")
      .attr("class", "dma-boundary")
      .attr("vector-effect","non-scaling-stroke")
      .attr("d", path);

    drawDay(orderedColumns[0], intab_data);  // initial map

    window.onresize = resize;
    resize();

    d3.select("#loader").remove();
  });
});


/* **************************************************
                     FUNCTIONS
************************************************** */

function drawDay(date) {
  // Update label in top left corner
  d3.select("#date p#month").html(date_format(new Date(date)));

  // For each path (DMA), log the intab rate for this date
  map.selectAll("path")
    .filter((d, i) => typeof(d) != "undefined")
    .style("fill", function(d, i) {
        if (typeof(d.data) === "undefined") {
          // this is for the DMAs we didn't collect data for
          return "lightgrey";
        } else {
          // for these, we did, so shade the area
          intab_value = d.data[date];
          scaled_value = intab_value / 2;
          return d3.interpolateRdBu(scaled_value);
        }
    });

}

function createSlider(){
  sliderScale = d3.scale.linear().domain([0,orderedColumns.length-1]);
  var val = slider ? slider.value() : 0;

  slider = d3.slider()
    .scale( sliderScale )
    .on("slide",function(event,value){
      if ( isPlaying ){
        clearInterval(interval);
      }
      currentFrame = value;
      drawDay(orderedColumns[value]);
      console.log(value, orderedColumns[value]);
    })
    .on("slideend",function(){
      d3.select("#slider-div").on("mousemove",sliderProbe)
    })
    .on("slidestart",function(){
      d3.select("#slider-div").on("mousemove",null)
    })
    .value(val);

  d3.select("#slider-div").remove();

  d3.select("#slider-container")
    .append("div")
    .attr("id","slider-div")
    .style("width",dateScale.range()[1] + "px")
    .on("mousemove",sliderProbe)
    .on("mouseout",function(){
      d3.select("#slider-probe").style("display","none");
    })
    .call( slider );

  d3.select("#slider-div a").on("mousemove",function(){
    d3.event.stopPropagation();
  })

  var sliderAxis = d3.svg.axis()
    .scale( dateScale )
    .tickValues( dateScale.ticks(orderedColumns.length) )
    .tickFormat(date_format)
    .tickSize(10);

  d3.select("#axis").remove();

  d3.select("#slider-container")
    .append("svg")
    .attr("id","axis")
    .attr("width",dateScale.range()[1] + sliderMargin*2 )
    .attr("height",25)
    .append("g")
    .attr("transform","translate(" + (sliderMargin+1) + ",0)")
    .call(sliderAxis);
}

function createLegend(){
  var legend = g.append("g").attr("id","legend").attr("transform","translate(560,10)");

  legend.append("circle").attr("class","gain").attr("r",5).attr("cx",5).attr("cy",10)
  legend.append("circle").attr("class","loss").attr("r",5).attr("cx",5).attr("cy",30)

  legend.append("text").text("jobs gained").attr("x",15).attr("y",13);
  legend.append("text").text("jobs lost").attr("x",15).attr("y",33);

  var sizes = [ 10000, 100000, 250000 ];
  for ( var i in sizes ){
    legend.append("circle")
      .attr( "r", circleSize( sizes[i] ) )
      .attr( "cx", 80 + circleSize( sizes[sizes.length-1] ) )
      .attr( "cy", 2 * circleSize( sizes[sizes.length-1] ) - circleSize( sizes[i] ) )
      .attr("vector-effect","non-scaling-stroke");
    legend.append("text")
      .text( (sizes[i] / 1000) + "K" + (i == sizes.length-1 ? " jobs" : "") )
      .attr( "text-anchor", "middle" )
      .attr( "x", 80 + circleSize( sizes[sizes.length-1] ) )
      .attr( "y", 2 * ( circleSize( sizes[sizes.length-1] ) - circleSize( sizes[i] ) ) + 5 )
      .attr( "dy", 13)
  }
}

function sliderProbe(){
  var d = dateScale.invert( ( d3.mouse(this)[0] ) );
  d3.select("#slider-probe")
    .style( "left", d3.mouse(this)[0] + sliderMargin + "px" )
    .style("display","block")
    .select("p")
    // .html( date_format(d) )
}

function resize(){
  var w = d3.select("#container").node().offsetWidth,
      h = window.innerHeight - 80;
  var scale = Math.max( 1, Math.min( w/width, h/height ) );
  svg
    .attr("width",width*scale)
    .attr("height",height*scale);
  g.attr("transform","scale(" + scale + "," + scale + ")");

  d3.select("#map-container").style("width",width*scale + "px");

  dateScale.range([0,500 + w-width]);
  
  createSlider();
}

function createDateScale(dates) {
  return d3.time.scale().domain([ 
    new Date(dates[0]), new Date(dates[dates.length - 1])
  ])
}
