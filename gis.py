
import os
import errno
import datetime as dt
import numpy as np
import pandas as pd
import geopandas as gpd
pd.set_option('display.width', 150)
pd.set_option('display.max_columns', 15)

import matplotlib as mpl
mpl.use('Qt5Agg')
import matplotlib.pyplot as plt
plt.ioff()
import imageio
import seaborn as sb
pal = sb.color_palette('deep')

from shapely.geometry.polygon import Polygon
from descartes import PolygonPatch

date_format = '%Y-%m-%d'

########################################################################################################################

event = 'florence'
date_range = ['2018-09-05', '2018-09-20']
label_dmas = [545, 550]
event_loc_fname = 'hurricaneFlorence_position_v_time.csv'

# event = 'michael'
# date_range = ['2018-10-04', '2018-10-20']
# label_dmas = [525, 656]
# event_loc_fname = 'hurricane michael.csv'

# event = 'campfire'
# date_range = ['2018-11-06', '2018-11-27']
# label_dmas = []

########################################################################################################################
def assert_dir_exists(path):
  """
  Checks if directory tree in path exists. If not it created them.
  :param path: the path to check if it exists
  """
  try:
    os.makedirs(path)
  except OSError as e:
    if e.errno != errno.EEXIST:
      raise

assert_dir_exists(event)

########################################################################################################################
### Read state shapes
states = gpd.read_file('cb_2017_us_state_500k/cb_2017_us_state_500k.shp')

########################################################################################################################
### Read dma shapes
dmas = gpd.read_file('nielsen_dma.geojson')
dmas['DMA'] = dmas['DMA'].astype('int')

### For each dma, get the outermost bounds in each dimension
dmas['xmin'] = np.nan
dmas['xmax'] = np.nan
dmas['ymin'] = np.nan
dmas['ymax'] = np.nan

for i in dmas.index:
  shape = dmas.loc[i, 'geometry']
  if isinstance(shape, Polygon):
    dmas.loc[i, 'xmin'] = min(shape.exterior.xy[0])
    dmas.loc[i, 'xmax'] = max(shape.exterior.xy[0])
    dmas.loc[i, 'ymin'] = min(shape.exterior.xy[1])
    dmas.loc[i, 'ymax'] = max(shape.exterior.xy[1])
  else:
    dma_xlims = [180, -180]
    dma_ylims = [180, -180]
    for p in shape:
      dma_xlims = [min(dma_xlims[0], min(p.exterior.xy[0])),
                   max(dma_xlims[1], max(p.exterior.xy[0]))]
      dma_ylims = [min(dma_ylims[0], min(p.exterior.xy[1])),
                   max(dma_ylims[1], max(p.exterior.xy[1]))]
    dmas.loc[i, 'xmin'] = dma_xlims[0]
    dmas.loc[i, 'xmax'] = dma_xlims[1]
    dmas.loc[i, 'ymin'] = dma_ylims[0]
    dmas.loc[i, 'ymax'] = dma_ylims[1]

dmas['xmid'] = dmas[['xmin', 'xmax']].mean(axis=1)
dmas['ymid'] = dmas[['ymin', 'ymax']].mean(axis=1)

########################################################################################################################
### Read hurricane data

if event in ['florence', 'michael']:
  event_locs = pd.read_csv(event_loc_fname)
  event_locs['dt_yr'] = event_locs['Date'] + ' 2018'
  event_locs['date'] = pd.to_datetime(event_locs['dt_yr'], format='%b %d %Y').dt.strftime(date_format)
  degree_symbol = event_locs['Lat'].iloc[0][-1]
  event_locs['lat'] = event_locs['Lat'].str.replace(degree_symbol, '').astype('float')
  event_locs['lon'] = event_locs['Lon'].str.replace(degree_symbol, '').astype('float')
elif event in ['campfire']:
  # create a row for campfire on each date
  event_locs1 = pd.DataFrame({'date': pd.date_range(dt.datetime(2018, 11, 8), dt.datetime(2018, 11, 25), freq='d')})
  event_locs1['lon'] = -121.44
  event_locs1['lat'] = 39.82
  ### Duplicate and add the Woolsey fire location for the same dates
  event_locs2 = pd.DataFrame({'date': pd.date_range(dt.datetime(2018, 11, 8), dt.datetime(2018, 11, 21), freq='d')})
  event_locs2['lon'] = -118.7013
  event_locs2['lat'] = 34.235
  ### Combine
  event_locs = pd.concat([event_locs1, event_locs2])
  event_locs['date'] = event_locs['date'].dt.strftime(date_format)

########################################################################################################################
### X/Y ranges
west_bound = max(event_locs['lon'].min(), dmas['xmin'].min())
east_bound = min(event_locs['lon'].max(), dmas['xmax'].max())

south_bound = max(event_locs['lat'].min(), dmas['ymin'].min())
north_bound = min(event_locs['lat'].max(), dmas['ymax'].max())

if event in ['campfire']:
  pad = 5
elif event in ['michael', 'florence']:
  pad = 1

xlims = [ west_bound-pad,  east_bound+pad]
ylims = [south_bound-pad, north_bound+pad]

plot_dmas = dmas.loc[ ( (dmas['xmax'] >= xlims[0])
                      & (dmas['xmin'] <= xlims[1]))
                    & ( (dmas['ymax'] >= ylims[0])
                      & (dmas['ymin'] <= ylims[1])
                      )
                    , 'DMA'].unique()

########################################################################################################################
### Get data metrics for each day/dma

sources = ['dtv', 'dish', 'charter']

day_sums = {}
for prv in sources:
  fname = '{}/{}_day_sums.csv'.format(event, prv)
  if not os.path.exists(fname):
    print('Creating {}'.format(fname))
    bucket_name = 'useast1-nlsn-w-digital-dsci-dev'
    local_filename = '/Users/wora6001/Code/hackathon-weather/{prv}_date_dma_summary.csv'
    s3_filename = 'users/wora6001/RPD_QA/summaries/{prv}/date_dma_summary.csv'
    try:
      (spark.read.csv('s3://'+bucket_name + '/' + s3_filename.format(prv=prv), header=True)
       .filter( F.col('date').between(*date_range)
                )
       .toPandas().sort_values(['dma_id', 'date'])
       .to_csv(fname)
      )
    except:
      import pyspark.sql.functions as F
      from pyspark.sql import SparkSession
      spark = (SparkSession
               .builder
               .config("spark.databricks.service.client.enabled", "true")
               .config("spark.databricks.service.address", "https://nielsen-prod.cloud.databricks.com")
               .config("spark.databricks.service.token", "dapi20e4836cce12c59edd476814cad8903b")
               .config("spark.databricks.service.clusterId", "0118-200422-ere136")
               .getOrCreate()
              )
      (spark.read.csv('s3://'+bucket_name + '/' + s3_filename.format(prv=prv), header=True)
       .filter( F.col('date').between(*date_range)
              )
       .toPandas().sort_values(['dma_id', 'date'])
       .to_csv(fname)
      )
  day_sums[prv] = pd.read_csv(fname, index_col=0)

########################################################################################################################
### Plot each DMA in the region
from mpl_toolkits.axes_grid1 import make_axes_locatable
### Color range
norm = mpl.colors.Normalize(vmin=0, vmax=2)
cmap = 'bwr'
cmap_transform = mpl.cm.ScalarMappable(cmap=cmap, norm=norm)

### Make one plot for each date
for d in pd.date_range(dt.datetime.strptime(date_range[0], date_format),
                       dt.datetime.strptime(date_range[1], date_format),
                       freq='d'):
  today = d.strftime(date_format)
  f1, ax1 = plt.subplots(2, 2, figsize=[12, 10])
  ax2 = ax1.flatten()
  f1.subplots_adjust(right=0.8)
  cbar_ax = f1.add_axes([0.90, 0.08, 0.04, 0.82])
  cb1 = mpl.colorbar.ColorbarBase(cmap=cmap, norm=norm, ax=cbar_ax)
  for i, prv in enumerate(sources):
    ax2[i].set_aspect('equal')
    ax2[i].set_title(prv)
    ### Plot each DMA in a color representing the data level
    for row, ind in enumerate(dmas.loc[dmas['DMA'].isin(plot_dmas)].index):
      shape = dmas.loc[ind, 'geometry']
      dma   = dmas.loc[ind, 'DMA']
      if dma in day_sums[prv]['dma_id'].unique():
        clr = cmap_transform.to_rgba(day_sums[prv].loc[ (day_sums[prv]['dma_id'] == dma)
                                                      & (day_sums[prv]['date']   == today),
                                                      'min_daytime_intb_frac_smooth'].iloc[0])
      else:
        clr = 'lightgray'
      if isinstance(shape, Polygon):
        polygons = [shape]
      else:
        polygons = list(shape)
      for p in polygons:
        ax2[i].add_patch(PolygonPatch(p, facecolor=clr, edgecolor='gray', lw=0.5, zorder=1))
    ### Label select DMAs
    for dma in label_dmas:
      row = dmas.loc[dmas['DMA']==dma].index[0]
      ax2[i].text(s=str(dma), x=dmas.loc[row, 'xmid'], y=dmas.loc[row, 'ymid'],
                  color='white', weight='bold',
                  zorder=3, fontsize=10, horizontalalignment='center', verticalalignment='center')
      ax2[i].text(s=str(dma), x=dmas.loc[row, 'xmid'], y=dmas.loc[row, 'ymid'],
                  zorder=3, fontsize=10, horizontalalignment='center', verticalalignment='center')
    ### Plot the state boundaries
    for row in states.index:
      shape = states.loc[row, 'geometry']
      if isinstance(shape, Polygon):
        polygons = [shape]
      else:
        polygons = list(shape)
      for p in polygons:
        ax2[i].plot(*p.exterior.xy, color='black', lw=0.5, zorder=2)
    ### Plot the hurricane's location(s) on this date
    ax2[i].scatter(event_locs.loc[event_locs['date'] == today, 'lon'],
                   event_locs.loc[event_locs['date'] == today, 'lat'],
                   s=40, c='orange', zorder=3)
    if event in ['michael', 'florence']:
      ax2[i].plot(event_locs['lon'], event_locs['lat'], lw=2, c='orange', zorder=2)
      ax2[i].plot(event_locs.loc[event_locs['date'] == today, 'lon'],
                  event_locs.loc[event_locs['date'] == today, 'lat'],
                  lw=4, c='purple', zorder=3)
    ax2[i].set_xlim(*xlims)
    ax2[i].set_ylim(*ylims)
  f1.text(s=today, x=0.5, y=0.95, fontsize=20, horizontalalignment='center', verticalalignment='center')
  plot_fname = '{}/{}.png'.format(event, today)
  f1.savefig(plot_fname)
  plt.close('all')

### Blank png for the end of the gif
from PIL import Image
im1 = Image.open(plot_fname)
width, height = im1.size
im2 = Image.new('RGB', (width, height), (255, 255, 255))
im2.save("{}/zzz.png".format(event), "PNG")

#### Gif!
images = []
imgs = os.listdir(event)
imgs.sort()
for file_name in imgs:
    if file_name.endswith('.png'):
        file_path = os.path.join(event, file_name)
        images.append(imageio.imread(file_path))
imageio.mimsave('{e}/{e}_animated.gif'.format(e=event), images, duration=1)
